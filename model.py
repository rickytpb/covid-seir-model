import numpy as np 
import matplotlib.pyplot as plt
import pandas as pd
import argparse
import sys

LENGTH = 365

def seir(k1,y1,x):
    beta = 0.05
    gamma = 0.05
    sigma = 0.1
    k1[0] = -1.*beta*y1[0]*y1[2]/(sum(y1))
    k1[1] = beta*y1[0]*y1[2]/(sum(y1)) - sigma*y1[1]
    k1[2] = sigma*y1[1] - gamma*y1[2]
    k1[3] = gamma*y1[2]

def sir(k1,y1,x):
    beta = 0.05
    gamma = 0.05
    k1[0] = -1.*beta*y1[0]*y1[1]/(sum(y1)) 
    k1[1] = beta*y1[0]*y1[1]/(sum(y1)) - gamma*y1[1]
    k1[2] = gamma*y1[1]
    
def sis(k1,t1,x):
    beta = 0.05
    gamma = 0.05
    k1[0] = -1.*beta*y1[0]*y1[1]/(sum(y1)) + gamma*y1[1]
    k1[1] = beta*y1[0]*y1[1]/(sum(y1)) - gamma*y1[1]


def rk4(y1,k1,x,h,f):
    k2 = np.empty(len(k1))
    k3 = np.empty(len(k1))
    k4 = np.empty(len(k1))
    y = y1+h*k1/2.
    f(k2,y,x+h/2.)
    y = y1+h*k2/2
    f(k3,y,x+h/2.)
    y = y1+h*k3
    f(k4,y1,x+h)
    y1 += h*(k1/6.+k2/3.+k3/3.+k4/6.)
    f(k1,y1,x+h)
    x += h
    return x


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='COVID-19 model solver')
    parser.add_argument('-m','--model', help='Choose type of model (SIR,SIS,SEIR)', required=True)
    arg = parser.parse_args()
    if arg.model.upper() == "SIR":
        f=sir
        y1 = np.array([35.5,1.37,1.1])
        leg = ["Susceptible","Infectious","Recovered"]
    elif arg.model.upper() == "SIS":
        f=sis
        y1 = np.array([35.5,1.37])
        leg = ["Susceptible","Infectious"]
    elif arg.model.upper() == "SEIR":
        f = seir
        y1 = np.array([35.5,0.085,1.37,1.1])
        leg = ["Susceptible","Exposed","Infectious","Recovered"]
    else:
        print("Please choose a valid model option (SIR,SIS,SEIR)")
        sys.exit(1)
    
    x = 0.
    k1 = np.empty(len(y1))
    t = []
    data = np.empty([0,len(y1)])
    f(k1,y1,x)
    while x <LENGTH:
        t.append(x)
        data = np.vstack((data,y1))
        x = rk4(y1,k1,x,0.1,f)
    
    for col in data.T:
        plt.scatter(t,col,s=1)
     
    plt.title("Modeling COVID-19 with %s model" % arg.model.upper())
    plt.xlabel("Day (t)")
    plt.ylabel("Population (millions)")
    plt.legend(leg)
    plt.xlim(0,LENGTH)
    plt.ylim(bottom=0)
    
